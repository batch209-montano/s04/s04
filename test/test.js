const {assert} = require('chai');
const {getCircleArea,getNumberOfChar} = require('../src/util.js')

describe('test_get_circle_area',()=>{

	it('test_area_of_circle_radius_15_is_706.86',()=>{

		let area = getCircleArea(15);
		assert.equal(area,706.86);

	})

	it('test_area_of_circle_neg1_radius_is_undefined',()=>{
		let area = getCircleArea(-1);
		assert.isUndefined(area);
	})

	it('test_area_of_circle_0_radius_is_undefined',()=>{
		let area = getCircleArea(0);
		assert.isUndefined(area);
	})

/*
	Mini-Activity

	Using the tdd process add another failing test to test a scenario if the user
	enters data that is not a number.
		-Assert the are to be undefined.
	Refactor the getCircleArea method to accomodate the scenario.
*/

    it('test_area_of_circle_string_radius_is_undefined',()=>{

    	let area = getCircleArea("25");
    	assert.isUndefined(area);

    })

    it('test_area_of_circle_invalid_type_is_undefined',()=>{

    	let area = getCircleArea({number: 25});
    	assert.isUndefined(area);

    })

})


describe("test_get_number_of_char_in_string",()=>{


	it("test_number_of_l_in_string_is_3",()=>{

		let numChar = getNumberOfChar("l","Hello World");
		assert.equal(numChar,3);

	})

	it("test_number_of_a_in_string_is_2",()=>{
		let numChar = getNumberOfChar("a","Malta");
		assert.equal(numChar,2);
	})

	/*
		Mini-Activity

		Using the tdd process, add 2 failing tests to test and develop for a scenario
		wherein a user inputs a non-string first argument and a scenario where a non-string argument was given as second argument.
			-Assert that numChar is undefined in both tests.

		Refactor and update the getNumberChar() to accomodate for both scenario and pass the tests.

		Send a screenshot of your passed tests in the hangouts.

	*/

	it("test_number_of_char_first_arg_non_string_is_undefined",()=>{

		let numChar = getNumberOfChar(true,"Javascript is fun!")
		assert.isUndefined(numChar);

	})

	it("test_number_of_char_second_arg_non_string_is_undefined",()=>{

		let numChar = getNumberOfChar("x",false)
		assert.isUndefined(numChar);

	})

})