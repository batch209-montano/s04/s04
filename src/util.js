function getCircleArea(radius){

	if(typeof radius !== "number"){
		return undefined;
	}

	if(radius <= 0){
		return undefined;
	} 

	return 3.1416*(radius**2);

}

function getNumberOfChar(char,string){

	//if char is a string we will proceed
/*	if(typeof char !== 'string'){
		return undefined;
	}
	//if string parameter is a string we will proceed
	if(typeof string !== 'string'){
		return undefined;
	}*/

	if(typeof char !== 'string' || typeof string !== 'string'){
		return undefined;
	}


	//transform the string into an array of characters so we can check the given char parameter against each character in the string parameter.
	//.split("") - splits the characters in a string and returns them in a new array
	let characters = string.split("");
	//console.log(characters);
	//loop over each Item in the characters array. IF the char param matches with the current character being iterated we will increment the counter variable.
	let counter = 0; 
	characters.forEach(character => {

		if(character === char){
			counter++;
		}

	})
	//log the counter in the console
	//console.log(counter);
	return counter;

}

let users = [
	
	{
		username: "brBoyd87",
		password: "87brandon19"
	},
	{
		username: "tylerOfsteve",
		password: "stevenstyle75"
	}

]

module.exports = {
	getCircleArea,
	getNumberOfChar,
	users
}