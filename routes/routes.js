const { users } = require('../src/util.js');
const express = require("express");
const router = express.Router();

const app = express();
app.use(express.json());

//Activity
// 1. In your routes, Add a new route provided by your instructor.

app.post('/register', (req, res) => {
	return res.send(register);
});

router.post('/register', (req, res) => {
	if(!req.body.hasOwnProperty('name')){
		return res.status(400).send({
			'error' : 'Bad Request : missing required parameter NAME'
		})
	}
	
	if(!req.body.hasOwnProperty('age')){
		return res.status(400).send({
			'error' : 'Bad Request: missing required parameter AGE'
		})
	}

	if(!req.body.hasOwnProperty('username')){
		return res.status(400).send({
			'error' : 'Bad Request: missing required parameter USERNAME'
		})
	}

	if(typeof req.body.age !== 'number'){
		return res.status(400).send({
			'error' : 'Bad Request: AGE has to be a number'
		})
	}

	if(typeof req.body.name !== 'string'){
		return res.status(400).send({
			'error' : 'Bad Request : NAME has to be string' 
		})
	}

})

// 1. In your routes, Add a new route provided by your instructor.

app.post('/login', (req, res) => {
	return res.send(login);
});

router.post('/login',(req,res)=>{

	let foundUser = users.find((user) => {

		return user.username === req.body.username && user.password === req.body.password

	});

	if(!req.body.hasOwnProperty('username')){
		return res.status(400).send({
			'error' : 'Bad Request: missing required parameter USERNAME'
		})
	}

	if(!req.body.hasOwnProperty('password')){
		return res.status(400).send({
			'error' : 'Bad Request: missing required parameter PASSWORD'
		})
	}

	if(foundUser){
		return res.status(200).send({
			'success': 'Thank you for logging in.'
		})
	}

	if(!foundUser){
		return res.status(403).send({
			'forbidden': 'Wrong Credentials.'
		})
	}

})


module.exports = router;
